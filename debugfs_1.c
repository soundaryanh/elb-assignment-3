#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/debugfs.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/uaccess.h>


#define MAX_SIZE 64

struct dentry *pdir;
struct dentry *pfile;

int fileValue;

unsigned char kbuffer;

int wr_offset=0,rd_offset=0;
int buflen=0;

 
/* read file operation */
static ssize_t debugfs_reader(struct file *fp, char __user *user_buffer, 
                                size_t size, loff_t *off) 
{ 
   int ret,rcount;
   
   if(buflen==0)	
   {
     printk("\nBuffer is empty\n");
     return 0;
   }
   
   rcount=size;	
   if(rcount>buflen)	
    rcount=buflen;
    
   printk("\nBefore Reading...");
   printk("\nSize:%d\nBuflen:%d\nRcount:%d\nRead_offset:%d\n",size,buflen,rcount,rd_offset);
   ret=copy_to_user(user_buffer,kbuffer+rd_offset,rcount);
   if(ret)
   {
     printk("\nRead from buffer failed\n");
     return -EFAULT;
   }
   
   rd_offset+=rcount;	//update the rd_offset
   buflen-=rcount;	//update the buflen
   
   printk("\nRead operation success\n");
   printk("\nAfter Reading...");
   printk("\nSize:%d\nBuflen:%d\nRcount:%d\nRead_offset:%d\n",size,buflen,rcount,rd_offset);	
   return rcount;
}
 
 
/* write file operation */
static ssize_t debugfs_writer(struct file *fp, const char __user *user_buffer, 
                                size_t size, loff_t *off) 
{ 

   int ret,wcount;
   if(wr_offset>=MAX_SIZE)		
   {
     printk("buffer is full no space to write\n");
     return -ENOSPC;
   }
   
   wcount =size;		
   if(wr_offset+size>MAX_SIZE)	
   wcount =MAX_SIZE-wr_offset;	
   printk("\nBefore Writing...");
   printk("\nSize:%d\nBuflen:%d\nWcount:%d\nWrite_offset:%d\n",size,buflen,wcount,wr_offset);
   ret=copy_from_user(kbuffer+wr_offset,user_buffer,wcount);
   if(ret)
   {
     printk("Copy from user failed\n");
     return -EFAULT;
   }
   
   wr_offset+=wcount;	//update the wr_offset
   buflen+=wcount;	//update the buflen
   
   printk("\nAfter Writing...");
   printk("\nSize:%d\nBuflen:%d\nWcount:%d\nWrite_offset:%d\n",size,buflen,wcount,wr_offset);	
   
   printk("\nSuccessfully written the content\n");
   return wcount;
} 


static const struct file_operations fops_debug = { 
  .read = debugfs_reader, 
  .write = debugfs_writer, 
}; 
 


static int __init dfs_demo_init(void) 	//init_module
{        
  pdir=debugfs_create_dir("psample", NULL);
  pfile=debugfs_create_file("pstatus",0644,pdir, &fileValue, &fops_debug);
  printk("Hello World..Welcome\n");
  
   kbuffer=kmalloc(MAX_SIZE,GFP_KERNEL);
    if(kbuffer==NULL)
    {
      printk("\nCouldn't allocate memory\n");
      return -ENOMEM;
    }
  return 0;
}


static void __exit dfs_demo_exit(void)       //cleanup_module
{
  //debugfs_remove(pfile);
  //debugfs_remove(pdir);
  kfree(kbuffer);
  //kfifo_free(&myfifo);
  debugfs_remove_recursive(pdir); 
  printk("Bye,Leaving the world\n");
}

module_init(dfs_demo_init);
module_exit(dfs_demo_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Your Name");
MODULE_DESCRIPTION("A Hello, World Module with debugging");

